let Bounds = {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,

    Update: function (x, y) {
        this.top = Math.min(y, this.top);
        this.right = Math.max(x, this.right);
        this.bottom = Math.max(y, this.bottom);
        this.left = Math.min(x, this.left);
    },

    Clean: function () {
        this.top = 999;
        this.right = 0;
        this.bottom = 0;
        this.left = 999;
    }
};

class WordObj {
    constructor(stringValue) {
        this.string = stringValue
        this.char = stringValue.split("")
        this.totalMatches = 0
        this.effectiveMatches = 0
        this.successfulMatches = []
    }
}

class Crossword {
    constructor(root = "crossword") {
        this.root = root
        this.prefix = 'crossword-'

        this.questions = [];
        this.board = [];
        this.wordBank = [];
        this.wordsActive = [];
    }

    prepareCrossword(words) {
        // Clear crossword
        Bounds.Clean();
        this.wordBank = [];
        this.wordsActive = [];
        this.board = Array(32).fill().map(() => Array(32).fill(null));

        // Create WordObj instances and store them in wordBank
        this.wordBank = words.map(word => new WordObj(word));

        // Iterate through all pairs of different WordObjs
        for (let wordA of this.wordBank) {
            for (let charA of wordA.char) {
                for (let wordB of this.wordBank) {
                    // Continue if wordA and wordB are the same word
                    if (wordA === wordB) continue;

                    // Increase totalMatches for every match of charA in wordB
                    for (let charB of wordB.char) {
                        wordA.totalMatches += charA === charB ? 1 : 0;
                    }
                }
            }
        }
    }

    // Function to create and return a crossVal array.
    // If the calculated index is out of the board bounds, it returns null.
    createCrossVal(curCross, m, isHorizontal) {
        let board = this.board
        // Calculate the index depending on the orientation
        const index = isHorizontal ? curCross.x + m : curCross.y + m;

        // Check if index is within board boundaries
        if (index < 0 || index > (isHorizontal ? board.length : board[curCross.x].length)) {
            return null;
        }

        // Initialize crossVal array
        let crossVal = [];

        // Get appropriate cell values from board depending on orientation
        const coords = isHorizontal ? [board[index][curCross.y], board[index][curCross.y + 1], board[index][curCross.y - 1]]
            : [board[curCross.x][index], board[curCross.x + 1][index], board[curCross.x - 1][index]];
        // Populate crossVal array with values from the board
        crossVal.push(...coords);

        // Return the crossVal array
        return crossVal;
    }

    // Function to check the values in the crossVal array against provided conditions.
    checkCrossVal(crossVal, m, lenM, curWord) {
        // Different checks based on the position of m
        return m > -1 && m < lenM - 1
            ? crossVal[0] !== curWord.char[m] && (crossVal[0] !== null || crossVal[1] !== null || crossVal[2] !== null) // Checks if any crossVal value is not null when m is within lenM bounds and crossVal[0] is not the current character
            : crossVal[0] !== null; // Checks if the first crossVal value is not null
    }

    addWordToCrossword() {
        let curWord, curChar, testWord, testChar,
            minMatchDiff = 9999, curMatchDiff;
        let wordBank = this.wordBank
        let wordsActive = this.wordsActive
        let board = this.board
        let len = wordBank.length
        let curIndex = 0
        if (wordsActive.length < 1) {
            for (let i = 0; i < len; i++) {
                if (wordBank[i].totalMatches < wordBank[curIndex].totalMatches) {
                    curIndex = i;
                }
            }
            wordBank[curIndex].successfulMatches = [{x: 12, y: 12, dir: 0}];
        } else {
            curIndex = -1;
            for (let i = 0; i < len; i++) {
                curWord = wordBank[i];
                curWord.effectiveMatches = 0;
                curWord.successfulMatches = [];
                let lenJ = curWord.char.length
                for (let j = 0; j < lenJ; j++) {
                    curChar = curWord.char[j];
                    let lenK = wordsActive.length
                    for (let k = 0; k < lenK; k++) {
                        testWord = wordsActive[k];
                        let lenL = testWord.char.length
                        for (let l = 0; l < lenL; l++) {
                            testChar = testWord.char[l];
                            if (curChar === testChar) {
                                curWord.effectiveMatches++;
                                let curCross = {x: testWord.x, y: testWord.y, dir: 0};
                                if (testWord.dir === 0) {
                                    curCross.dir = 1;
                                    curCross.x += l;
                                    curCross.y -= j;
                                } else {
                                    curCross.dir = 0;
                                    curCross.y += l;
                                    curCross.x -= j;
                                }

                                let isMatch = true;

                                // Calculate the length of the current word's characters plus 1
                                let lenM = curWord.char.length + 1;

                                // Loop over the characters of the current word
                                for (let m = -1; m < lenM; m++) {
                                    // If the current character index isn't the index of the match,
                                    // Then check the cells on the board for matching values
                                    if (m !== j) {
                                        // Generate crossVal for the current character
                                        let crossVal = this.createCrossVal(curCross, m, curCross.dir === 0);

                                        // If crossVal is null (indicating out of bounds) or the crossVal doesn't meet the conditions,
                                        // Then set isMatch to false and break the loop
                                        if (crossVal === null || this.checkCrossVal(crossVal, m, lenM, curWord)) {
                                            isMatch = false;
                                            break;
                                        }
                                    }
                                }

                                if (isMatch === true) {
                                    curWord.successfulMatches.push(curCross);
                                }
                            }
                        }
                    }
                }

                curMatchDiff = curWord.totalMatches - curWord.effectiveMatches;

                if (curMatchDiff < minMatchDiff && curWord.successfulMatches.length > 0) {
                    curMatchDiff = minMatchDiff;
                    curIndex = i;
                } else if (curMatchDiff <= 0) {
                    return false;
                }
            }
        }

        if (curIndex === -1) {
            return false;
        }

        let spliced = wordBank.splice(curIndex, 1);
        wordsActive.push(spliced[0]);

        let pushIndex = wordsActive.length - 1,
            rand = Math.random(),
            matchArr = wordsActive[pushIndex].successfulMatches,
            matchIndex = Math.floor(rand * matchArr.length),
            matchData = matchArr[matchIndex];

        wordsActive[pushIndex].x = matchData.x;
        wordsActive[pushIndex].y = matchData.y;
        wordsActive[pushIndex].dir = matchData.dir;
        len = wordsActive[pushIndex].char.length
        for (let i = 0; i < len; i++) {
            let xIndex = matchData.x,
                yIndex = matchData.y;
            if (matchData.dir === 0) {
                xIndex += i;
                board[xIndex][yIndex] = wordsActive[pushIndex].char[i];
            } else {
                yIndex += i;
                board[xIndex][yIndex] = wordsActive[pushIndex].char[i];
            }
            Bounds.Update(xIndex, yIndex);
        }
        return true;
    }

    createCrossword(words) {
        this.prepareCrossword(words)
        let isOk = true
        let len = this.wordBank.length
        for (let i = 0; i < len && isOk; i++) {
            isOk = this.addWordToCrossword()
        }
        return isOk;
    }

    getAllSymbols() {
        let board = this.board
        if (!board) return 0
        let count = 0;
        for (let i = 0; i < board.length; i++) {
            for (let j = 0; j < board[i].length; j++) {
                if (board[i][j]) {
                    count++
                }
            }
        }
        return count
    }
    validate() {
        let board = this.board
        let countErrors = 0
        let countAnswers = 0
        $(`.${this.prefix}square.${this.prefix}letter`).each((i, item) => {
            let value = $(item).find('input').val()
            if (value) {
                countAnswers++
                let i = parseInt($(item).attr('i').replace(' ', ''), 10)
                let j = parseInt($(item).attr('j').replace(' ', ''), 10)
                let check = board[j][i]
                $(item).removeClass(this.prefix + 'correct')
                $(item).removeClass(this.prefix + 'incorrect')
                if (value.toUpperCase() === check.toUpperCase()) {
                    $(item).addClass(this.prefix + 'correct')
                } else {
                    countErrors++
                    $(item).addClass(this.prefix + 'incorrect')
                }
            }
        })
        console.log('All symbols: ', this.getAllSymbols());
        console.log('countAnswers: ', countAnswers);
        console.log('countErrors: ', countErrors);
        return (countAnswers >= this.getAllSymbols() && countErrors === 0)
    }

    arrayToString(array, separator = ',') {
        // Return an empty string if array is null or empty
        if (array === null || array.length < 1) {
            return "";
        }

        // Use a comma as the default separator if it's null
        if (separator === null) {
            separator = ",";
        }

        // Initialize the result string with the first element of the array
        let resultString = array[0];

        // Concatenate each remaining element of the array with the separator
        for (let i = 1; i < array.length; i++) {
            resultString += separator + array[i];
        }

        // Return the result string
        return resultString;
    }
    renderHtmlElement(e, c, h) {
        h = (h) ? h : "";
        let s = "<" + e + " ";
        for (let i = 0; i < c.length; i++) {
            s += c[i].a + "='" + this.arrayToString(c[i].v, " ") + "' ";
        }
        return (s + ">" + h + "</" + e + ">");
    }
    renderCrosswordSymbol(c, j, i) {
        let arr = (c) ? [`${this.prefix}square`, `${this.prefix}letter`] : [`${this.prefix}square`]
        let options = [
            {
                a: 'class',
                v: arr
            },
            {
                a: 'j',
                v: j.toString()
            },
            {
                a: 'i',
                v: i.toString()
            },
        ]
        const words = this.wordsActive.filter(w => w.x === j && w.y === i)
        const questions = this.getCrosswordQuestions()
        let questionNumber = ''
        for (let i = 0; i < words.length; i++) {
           let word = words[i]
           let questionH = questions.horizontal.findIndex(q => q.word.toUpperCase() === word.string.toUpperCase())
           if (questionH > -1) {
              questionNumber += (questionNumber !== '')?'/':''
              questionNumber += (questionH + 1)
           }
           let questionV = questions.vertical.findIndex(q => q.word.toUpperCase() === word.string.toUpperCase())
           if (questionV > -1) {
              questionNumber += (questionNumber !== '')?'/':''
              questionNumber += (questionV + 1)
           }
        }
        if (questionNumber) {
           options.push({
               a: 'n',
               v: questionNumber.toString()
           })
        }
        return this.renderHtmlElement('div', options, c)
    }
    renderCrossword(play=false) {
        if (this.board.length === 0) {
            return
        }
        let board = this.board
        let str = '';
        for (let i = Bounds.top - 1; i < Bounds.bottom + 2; i++) {
            str += `<div class="${this.prefix}row">`;
            for (let j = Bounds.left - 1; j < Bounds.right + 2; j++) {
                str += this.renderCrosswordSymbol(board[j][i], j, i);
            }
            str += "</div>";
        }
        if (this.getAllSymbols() > 0) {
            document.getElementById(this.root).innerHTML = str
        } else {
            document.getElementById(this.root).innerHTML = "Failed to find crossword."
        }

        if (play) {
            let letters = document.getElementsByClassName(this.prefix + 'letter')
            for (let i = 0; i < letters.length; i++) {
                let letter = letters[i]
                let placeholder = ''
                if (letter.getAttribute('n')) {
                    placeholder = letter.getAttribute('n').replaceAll(' ', '')
                }
                letter.innerHTML = `<input class="${this.prefix}char" type="text" maxlength="1" placeholder="${placeholder}"/>`
            }
        }
    }

    generateCrossword(questions) {
        this.questions = questions
        let words = questions.map(item => item.word.toUpperCase())
        let isSuccess = false
        for (let i = 0; i < 10 && !isSuccess; i++) {
            isSuccess = this.createCrossword(words)
        }
        return isSuccess
    }

    getCrosswordQuestions() {
        let result = {
           'horizontal': [],
           'vertical': []
        }
        for (let i = 0; i < this.wordsActive.length; i++) {
            let word = this.wordsActive[i]
            const question = this.questions.find(q => q.word.toUpperCase() === word.string.toUpperCase());
            if (question) {
               if (word.dir === 0) {
                  result.horizontal.push(question)
               } else {
                  result.vertical.push(question)
               }
            }
        }
        return result
    }

    renderCrosswordQuestions(labelH = 'Horizontal', labelV = 'Vertical', root='crossword-questions') {
        let questions = this.getCrosswordQuestions()
        let str = '<div class="container">'
        str += '<div class="column">'
        str += `<h4>${labelH}</h4>`
        str += `<table>`
        for (let i = 0; i < questions.horizontal.length; i++) {
          str += `<tr><td>${i+1}</td><td>${questions.horizontal[i].description}</td></tr>`
        }
        str += `</table>`
        str += '</div>'
        str += '<div class="column">'
        str += `<h4>${labelV}</h4>`
        str += `<table>`
        for (let i = 0; i < questions.vertical.length; i++) {
            str += `<tr><td>${i+1}</td><td>${questions.vertical[i].description}</td></tr>`
        }
        str += `</table>`
        str += '</div>'
        str += '</div>'
        document.getElementById(root).innerHTML = str
    }
}
